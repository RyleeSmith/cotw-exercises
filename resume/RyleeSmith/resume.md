# Rylee Smith
_________________________________________________
- 1060 South Main St. Sheridan WY 82801 | (307)674-8896 |
- smith82801@bresnan.net

## About Me
_________________________________________________
I am a hard worker with strong moral ethics. I value punctuality, respect, and dedication to the task at hand.

## Computer Skills
_________________________________________________
##### Certifications:
- Dell Certification
- PC Pro Certification
- Networking Certification

## Experience
_________________________________________________
##### July 1, 2015 - February 2016
_Cashier, Wal-Mart_
- While being a cashier I had to be efficient with the cash register while being polite to the customer. A smile goes a long way.
##### June 6, 2016 - August 27, 2016
_IT Tech for School District #2 in Sheridan WY_
- While working for the School District I had to listen to co-workers about what is wrong and trubble shoot what is wrong and how I can fix the problem.

## Education
_________________________________________________
- Currently at Sheridan High School

# References
_________________________________________________
##### Emily Emond
- 1665 Edwards Dr.
- (307)731-2484
- Teacher

##### Stephanie Eisenhauer
- 120 Canvasback
- (307)674-8379
- Pharmacist

##### Shirley Coulter
- 1884 Summit Dr.
- (307)673-9110
- Teacher
